import asyncio
import aiohttp
import os

from os.path import join


class Quotes:
    def __init__(self):
        self.make_dir()


    def make_dir(self):
        try:
            path = join(os.getcwd(),'results')
            os.makedirs(path)

        except FileExistsError:
            pass


    def save_file(self,
                data, 
                ticker: str, 
                year: str) -> None:

        filename = 'results/%s-%s.csv' % (ticker, year)
        with open(filename, 'wb') as file:
            file.write(data)


    def get_url(self, 
                ticker: str, 
                year: str, 
                market: str): 

        dates = '%s/candles.csv'\
                '?from=%s-01-01'\
                '&till=%s-12-31'\
                '&interval=24' % (ticker, year, year)

        if market == 'eq':
            url = 'http://iss.moex.com/iss/engines/stock/markets/shares/securities/' + dates
                    
        elif market == 'fx':
            url = 'http://iss.moex.com/iss/engines/currency/markets/selt/boards/cets/securities/' + dates
        
        else:
            print('Choose eq or fx')

        return url


    async def fetch_data(self,
                        url: str, 
                        session: aiohttp.ClientSession, 
                        ticker: str, 
                        year: str) -> None:

        async with session.get(url) as response:
            data = await response.read()
            self.save_file(data, ticker, year)


    async def download(self,
                        tickers: list, 
                        years: list,
                        market: str) -> None:

        tasks = []

        async with aiohttp.ClientSession() as session:
            for ticker in tickers[market]:    
                for year in years:
            
                    url = self.get_url(ticker, year, market)
                    task = asyncio.create_task(self.fetch_data(url, session, ticker, year))
                    tasks.append(task)

            await asyncio.gather(*tasks)



if __name__ == "__main__":
    from time import time

    t0 = time()

    eq = ['AFKS','AFLT','ALRS','CHMF','FXCN','FXDE','FXGD','FXIT',
        'FXJP','FXMM','FXRB','FXRU','FXUS','GAZP','GMKN','HYDR',
        'IRAO','LKOH','MAGN','MGNT','MOEX','MTSS','NLMK','NVTK',
        'PLZL','POLY','ROSN','RSTI','SBER','SBERP','SIBN','SNGS',
        'SNGSP','TATN','TATNP','TRNFP','VTBR','YNDX']

    fx = ['EURUSD000TOM', 'EURUSD000TOD', 
        'USD000UTSTOM', 'USD000000TOD']

    tickers = { 'eq': eq ,
                'fx': fx }

    years = ['2015', '2016', '2017', '2018', '2019']
    
    Q = Quotes()
    asyncio.run(Q.download(tickers, years, 'eq'))
    asyncio.run(Q.download(tickers, years, 'fx'))

    t1 = time() - t0
    print('Success: %s sec.' % round(t1, 2))