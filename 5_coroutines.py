
def coroutine(func):
    def inner (*args, **kwargs):
        g = func(*args, **kwargs)
        g.send(None)
        return g
    return inner


@coroutine
def subgen():
    x = 'Ready to accept message'
    massage = yield x
    print ('Subgen recieved: ', massage)


@coroutine
def avg():
    count = 0
    summ = 0
    avg = None

    while True:
        try:
            x = yield avg
        except StopIteration:
            print ('Done')
            break
        else:
            count += 1
            summ += x
            avg = round(summ / count, 2)
    
    return avg