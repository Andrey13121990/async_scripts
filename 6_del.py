
def coroutine(func):
    def inner (*args, **kwargs):
        g = func(*args, **kwargs)
        g.send(None)
        return g
    return inner


class BlaBlaException(Exception):
    pass


def subgen():
    while True:
        try:
            massege = yield
        except StopIteration:
            break
        else:
            print('........', massege)

    return 'Returned from subgen()'


@coroutine
def delegator(g):
#    while True:
#        try:
#            data = yield
#            g.send(data)
#        except BlaBlaException as e:
#            g.throw(e)

    result = yield from g
    print(result)