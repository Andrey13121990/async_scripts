import pandas as pd
import datetime as dt

from typing import Union

from os import getcwd
from os.path import join
from time import time



class Price:

    def __init__(self, ticker: str) -> None:
        self.ticker = ticker


    def get_day_close(self, date: dt.date) -> Union[None, float]:
        
        day = date.strftime('%Y-%m-%d')

        url = 'http://iss.moex.com/iss/engines/stock/markets/shares/securities/'\
                '%s/candles.csv'\
                '?from=%s'\
                '&till=%s'\
                '&interval=24' % (self.ticker, day, day)
        
        df = pd.read_csv(url, ';', skiprows = 2)
        
        if df.empty:
            return None
        else:
            return float(df['close'])


    def get_dates(self, start: dt.date, end: dt.date) -> list:
        
        if end > start:
            dates = list()
            begin = start
            while begin < end:
                dates.append(begin)
                begin += dt.timedelta(days=1)     
        else: 
            dates = list()
        
        return dates


    def download_history(self, start: dt.date, end: dt.date) -> pd.DataFrame:
        
        history = dict()
        dates = self.get_dates(start, end)
        for item in dates:
            history[item] = self.get_day_close(item)

        return pd.DataFrame.from_dict(history, orient = 'index')


    def save_history(self, start: dt.date, end: dt.date):

        df = self.download_history(start, end)

        path = join(getcwd(),'results')
        file_path = join(path, '%s.csv' % self.ticker)
        
        df.to_csv(file_path)

        print('%s --- success' % self.ticker)


    
def main(ticker: str):
    start = dt.date(year = 2019, month = 2, day = 1)
    end = dt.date(year = 2019, month = 3, day = 28)

    Price(ticker).save_history(start, end)



if __name__ == "__main__":
    d0 = time()
    main('SBER')

    print(time() - d0)